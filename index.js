const express = require('express');
const { emitKeypressEvents } = require('readline');

const app = express();

const port = 8000;

class Drink {
    constructor(id, maNuocUong, tenNuocUong, donGia, ngayTao, ngayCapNhat) {
        this.id = id;
        this.maNuocUong = maNuocUong;
        this.tenNuocUong = tenNuocUong;
        this.donGia = donGia;
        this.ngayTao = ngayTao;
        this.ngayCapNhat = ngayCapNhat;
    };
};

const drink1 = new Drink(1, 'TRATAC', 'Trà tắc', 10000, '14/5/2021', '14/5/2021');
const drink2 = new Drink(2, 'COCA', 'Cocacola', 15000, '14/5/2021', '14/5/2021');
const drink3 = new Drink(3, 'PEPSI', 'Pepsi', 15000, '14/5/2021', '14/5/2021');

const gDrinkObjArr = [drink1, drink2, drink3];

const drinkArr = ['TRATAC', 'COCA', 'PEPSI'];

app.get('/drinks-class', (req, res) => {
    const vQueryCode = req.query; //{code: 1}
    let vResult = null;
    if (!vQueryCode.code) { // !vQueryCode.code nghĩa là 1 trong các trường hợp: null, undefined, '', 0, false, còn nếu không có ! đằng trước thì sẽ là tất cả các giá trị nằm ngoài 5 giá trị kia
        vResult = drinkArr;
    } else {
        let vCheck = false;
        let vIndex = 0;
        while (vCheck === false && vIndex < gDrinkObjArr.length) {
            if (vQueryCode.code == gDrinkObjArr[vIndex].id) {
                vResult = gDrinkObjArr[vIndex];
                vCheck = true;
            } else {
                ++vIndex;
            };
        };
    }
    res.json(vResult);
});

app.get('/drinks-object/:drinkId', (req, res) => {
    const vDrinkId = req.params.drinkId;
    let vResult = null;
    let vCheck = false;
    let vIndex = 0;
    if (!vDrinkId) {
        vResult = gDrinkObjArr;
    } else {
        while (vCheck === false && vIndex < gDrinkObjArr.length) {
            if (vDrinkId == gDrinkObjArr[vIndex].id) {
                vResult = gDrinkObjArr[vIndex];
                vCheck = true;
            } else {
                ++vIndex;
            };
        };
    }
    res.json(vResult);
});

app.get('/drinks-object', (req, res) => {
    res.json([drink1, drink2, drink3]);
});

app.listen(port, () => {
    console.log('server is listening on port:' + port);
});